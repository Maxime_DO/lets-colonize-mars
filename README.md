# Let's colonize Mars

## Liens & rendu:

* Mon [Backlog & Kanban](https://tree.taiga.io/project/maxime_do-lets-colonize-mars/timeline) Taïga
* Le [site en ligne](http://prototype-mars.herokuapp.com/) :rocket: ```Attention : la version actuelle ne possède pas encore de moyen pour supprimer une entrée```
* Les **maquettes & le schéma de base de données** se trouve dans le **dossier doc**.

## Pour lancer le projet en local: 

1. Cloner/télécharger le projet,

Depuis le **dossier mars**, écrire dans votre terminal:

2. ```composer install && npm install```

3. Avec un SGBD, type "*MySQL Workbench*", créer une BDD (connexion & schema) vide pour l'instant.

4. Dupliquer le ```.env.exemple```, renommer la copie en ```.env```, puis y renseigner nos informations.

Pour accèder à notre base de données, il faut renseigner :

    DB_DATABASE= *nom-de-la-BDD*
    DB_USERNAME= *nom-utilisateur-BDD*
    DB_PASSWORD= *mdp-BDD*


Il faut également définir la clés d'application, l'APP_KEY, cela se fait automatiquement avec la commande :

5. ```php artisan key:generate``` *(toujours depuis le dossier mars)*

On peut désormais initialiser les tables de la BDD:

6. ```php artisan migrate:fresh --seed```

Pour lancer le serveur local : 

```php artisan serve```