<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // ADMIN
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@mail.co',
            'password' => bcrypt('Password'),
        ]);
        DB::table('roles')->insert([
            'slug' => 'admin',
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '1',
        ]);

        // USER
        DB::table('users')->insert([
            'name' => 'user',
            'email' => 'user@mail.co',
            'password' => bcrypt('Password'),
        ]);
        DB::table('roles')->insert([
            'slug' => 'user',
        ]);
        DB::table('role_user')->insert([
            'user_id' => '2',
            'role_id' => '2',
        ]);

        // ORES
        DB::table('ores')->insert([
            'name' => 'Klingon',
            'description' => 'Le Klingon, un peu relou, altère les combinaisons, traverse les matériaux et provoque des démangeaisons cutanées,  On a observé une résistance chez les geeks.',
        ]);
        DB::table('ores')->insert([
            'name' => 'Chomdû',
            'description' => 'Le Chomdû est un minerai qui provoque des états dépressifs. Bizzarement, les personnes âgées ne sont plus affectées...',
        ]);
        DB::table('ores')->insert([
            'name' => 'Perl',
            'description' => 'e Perl est le minerai à éviter à tout prix ! Hystérie, dépression, folie, voir mort subite sont les effets fréquemment observés.',
        ]);

        // AREAS
        DB::table('areas')->insert([
            'name' => 'Le cercle de feu',
            'longitude' => '40.201234914847',
            'latitude' => '23.7434141341416',
            'lvl' => '7',
            'created_at' => Carbon::now()->addMonths(217)->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->addMonths(217)->format('Y-m-d H:i:s'),
        ]);
        DB::table('area_ore')->insert([
            'area_id' => '1',
            'ore_id' => '1',
            'created_at' => Carbon::now()->addMonths(210)->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->addMonths(210)->format('Y-m-d H:i:s'),
        ]);
        // $this->call(UsersTableSeeder::class);
    }
}
