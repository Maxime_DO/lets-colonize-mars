<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    /**
    * The storage format of the model's date columns.
    *
    * @var string
    */
    // protected $dateFormat = 'D, d M Y H:i:s';
    
    /**
     * The ores that belong to the area.
     */
    public function ores()
    {
        return $this->belongsToMany('App\Ore');
    }
}
