<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ore extends Model
{
    /**
     * The areas that belong to the ore.
     */
    public function areas()
    {
        return $this->belongsToMany('App\Area');
    }
}
