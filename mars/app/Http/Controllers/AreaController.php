<?php

namespace App\Http\Controllers;

use App\Ore;
use App\Area;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areas = Area::all();
        $ores = Ore::all();

        return view ('index', [
            'areas' => $areas,
            'ores' => $ores,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $area = new Area;
        $area->name      = $request->input('name');
        $area->lvl       = $request->input('lvl');
        $area->longitude = $request->input('longitude');
        $area->latitude  = $request->input('latitude');
        // $area->description = $request->input('description');
        
        $area->save();
        
        // Area::find($area->id)->ores()->save($ore_id);  // Ore is saved to existing area
        
        // Foncitonnel mais à modifier :
        $area_id = Area::find($area->id)->id;
        $ore_id = $request->input('ore_id');

        DB::table('area_ore')->insert(
            ['area_id' => $area_id, 'ore_id' => $ore_id]
        );

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
