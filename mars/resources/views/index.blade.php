@extends('layouts.app')

@section('content')
<div class="w-75 mx-auto">
  {{-- form container flex --}}
  <div class="d-flex justify-content-center">
    {{-- form add area --}}
    <div>
      <h2>Signaler une zone <br>dangereuse</h2>
      <form class="d-flex flex-column border border-dark rounded mr-5" action="/add/area" method="POST">
        @csrf
        <label for="name">Nom de zone: <input required class="form-control" name="name" id="name" type="text"></label>
        <label for="ore_id">Minerai: 
          <select class="form-control" required name="ore_id" id="ore_id">
            <option hidden value="">Selectionne un minerai</option>
            @foreach ($ores as $ore)
              <option value="{{ $ore->id }}">{{ $ore->name }}</option>    
            @endforeach
          </select>
        </label>
        <label for="lvl"><abbr title="Niveau de dangerosité">NDD: </abbr>
          <select class="form-control" required name="lvl" id="lvl">
            <option hidden value="">Niveau de dangerosité</option>
            <optgroup label="C'est cool">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </optgroup>
            <optgroup label="C'est moyen cool">
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
            </optgroup>
            <optgroup label="C'est pas cool du tout">              
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
            </optgroup>
          </select>
        </label>
        <label for="longitude">Longitude: <input class="form-control" required name="longitude" id="longitude" type="number" step="0.00000000000001"></label>
        <label for="latitude">Latitude: <input class="form-control" required name="latitude" id="latitude" type="number" step="0.00000000000001"></label>
        {{-- <label for="description">Description<input name="description" id="description" type="text"></label> --}}
        <button class="btn btn-lg btn-outline-danger" type="submit">Ajouter la zone</button>
      </form>
    </div>
      <div>
        <h2>Enregistrer un nouveau <br>minerai</h2>
        {{-- form add ore --}}
        <form class="d-flex flex-column border border-dark rounded" action="/add/ore" method="POST">
          @csrf
          <label for="name">Nom: <input required class="form-control" name="name" id="name" type="text"></label>
          <label for="description">Description: <textarea required class="form-control" name="description" id="description" rows="5"></textarea></label>
          <button class="btn btn-lg btn-outline-danger" type="submit">Ajouter le minerai</button>
        </form>
      </div>
    <div>

    </div>
  </div>

  <hr>
  
  {{-- TABLEAU --}}
  <div class="d-flex justify-content-around">
    <table>
      <thead>
        <tr>
          <th colspan="1">Zone</th>
          <th colspan="1">Minerai</th>
          <th colspan="1">NDD</th>
          <th colspan="1">Longitude</th>
          <th colspan="1">Latitude</th>
          <th colspan="1">Posté le</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($areas as $area)
        <tr>
          <td>{{ $area->name }}</td>
          <td>
            @foreach ($area->ores as $ore)
                {{$ore->name}}
            @endforeach
          </td>
          <td>{{ $area->lvl }}</td>
          <td>{{ $area->longitude }}</td>
          <td>{{ $area->latitude }}</td>
          <td>{{ $area->updated_at ?? $area->created_at ?? '' }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  {{-- FIN TABLEAU --}}
</div>
@endsection
