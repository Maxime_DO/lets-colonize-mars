<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/', function () {
//     return view('auth.login');
// });

Route::get('/', 'AreaController@index');   // home
Route::get('/user', 'UserController@index');    // test

Route::post('/add/area', 'AreaController@store');
Route::post('/add/ore', 'OreController@store');
